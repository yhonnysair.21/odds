- 2021
In the last session of 2021, the Legislature of the Province of Córdoba in Argentina
has debated and approved Law No. 10793, which aims to regulate online gambling,
in order to guarantee public order,eradicate illegal gambling and save the
rights of those who participate in them.
